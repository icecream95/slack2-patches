#!/bin/bash

# Copyright (C) Icecream95 2019m BSD-3-Clause License

# This is a script to do some basic fixups on source files.

if ! [ -e "$1" ]; then
    exit 1
fi

chmod +w "$1"

sed -i 's/extern[ \t]*char[ \t]*\* *sys_errlist/extern const char *const sys_errlist/' "$1"
sed -i 's/extern[ \t]*int[ \t]*errno;/#include <errno.h>/' "$1"
