# Slackware 2.0 patches for modern GNU/Linux

This is an in-progress set of patches to allow compiling many of the
packages of Slackware 2.0.1 (available [here][1], in the slacksrc
directory) on modern Linux systems.

You should note that a lot of the programs long since gone from other
distributions are still in Slackware, so if you actually want to use
some of these applications current Slackware versions [sources here][2]
will have better maintained versions.

After extracting the source for a package, you can patch it by going
into the source directory and using `patch -p1`.

For example, to extract and patch xpaint, you would run

```bash
cd /path/to/slackware-2.0.1/slacksrc/xap/xpaint/
tar -xvf xpaint-2.1.1.tar.gz
cd xpaint
patch -p1 -i ~/path/to/slack2-patches/xap_xpaint.patch
```

Files such as README or INSTALL should give you clues on how to
compile the packages. In most cases it's just a `make`, but in some
cases you'll need to run `./configure` or some other configuration
script first. Note that for X applications, you do not have to run
xmkmf, you can just type make to compile them. If the configure step
prompts you on anything, you can accept the defaults, but you might
want to change some of the paths.

Note that the install step is not tested, so you will hit problems
trying to install many of these programs.

Some patches may create a README.patched file, which will give you
more information on compiling that program. There might also be notes
at the top of the diff in cases you might have trouble applying it.

These patches are under the BSD 3-Clause License if you feel you need
an explicit copyright grant, but remember many of the programs
themselves are under non-free licenses or simply no license at all.

[1]: https://web.archive.org/web/20150520230816/http://www.nielshorn.net/_download/slackware/old/slackware-2.0.1.tar.bz2
[2]: https://mirrors.slackware.com/slackware/slackware64-current/source/
